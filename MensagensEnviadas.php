<?php
// require_once 'app/appConfig.php';
require_once 'bootstrap.php';
require_once 'menu.php';
ExigeRemetente();
if (isset($_SESSION['dadosEmail']['emailsEnviados']) AND $_SESSION['dadosEmail']['emailsEnviados'] != "") {
	echo "Sucesso!<br>";
	//echo nl2br($_SESSION['dadosEmail']['emailsEnviados']);
	MostraFlashMessage();
	unset($_SESSION['dadosEmail']['emailsEnviados']);
} else {
	echo "Erro!<br>Mensagens não enviadas, verifique os dados informados (remetente e servidor) ou consulte os logs.";
	echo "<br>";
	echo "
<a href='" . ROOTAPP . "Listar_Emails_Cadastrados.php'>Emails_Cadastrados</a> |
<a href='" . ROOTAPP . "Cadastrar_Remetente.php'>Cadastrar_Remetente</a> |
<a href='" . ROOTAPP . "Mostrar_Remetente.php'>Mostrar_Remetente</a>
";
}

?>
