<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Enviar Arquivo CSV</title>
</head>
<body>
</body>
<?php
// require_once 'app/appConfig.php';
require_once 'bootstrap.php';
require_once 'menu.php';
ExigeRemetente();
$caminhoArquivo = './pastacsv/novoListaDeEmails.csv';

if (is_writable($caminhoArquivo)) {
	?>
	<h5>Enviar Arquivo CSV</h5>
	<h5>Tenha em mente que ao enviar um  novo arquivo, este substituirá o antigo</h5>
	<span>Tenha certeza de que o arquivo CSV segue o padrão <strong>"Nome; Email; Mensagem(opcional)"</strong>  separado por linha como abaixo:</span>
	<pre>
		Nome do dewstinatário;email@servidor.com;mensagem
		Nome do dewstinatário;email@servidor.com;mensagem
	</pre>
	<p>
		Baixar <a href="listaExemplo.csv" target="_blank">listaExemplo.csv</a>
	</p>
	<hr>
	<form action="salvaCSV.php" enctype="multipart/form-data" method="POST">
		Escolha o arquivo:
		<br>
		<input name="arquivo" type="file">
		<br>
		<br>
		<input type="submit" value="Upload" onclick="if (! confirm('Este arquivo substituirá a lista atual. Continuar?')) { return false; }">
	</form>
	<hr>
<?php } else {
	echo "Verifique as permissões de gravação no arquivo de lista de e-mail.";
}?>
</body>
</html>