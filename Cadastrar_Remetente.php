<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Cadastrar Remetente</title>
</head>
<body>
<?php
// require_once 'app/appConfig.php';
require_once 'bootstrap.php';
require_once 'menu.php';
?>
	<h4>Cadastrar Remetente</h4>
<?php

echo "<h6>";
MostraFlashMessage();
echo "</h6>";

if (isset($_SESSION['dadosEmail'])) {
	echo "<p>";
	echo "Já há um remetente cadastrado nesta sessão.";
	echo " <a href='Mostrar_Remetente.php'>Ver Remetente Cadastrado</a> | ";
	echo " <a href='Sair.php'>Sair</a> ";
	echo "<br>";
	echo "Os dados informados abaixo servem para autenticar no seu provedor de e-mail.<br>Nenhum dado fica salvo neste servidor, todos eles serão limpos em alguns minutos. ";
	echo "</p>";
}

$remNome = (isset($_SESSION['dadosEmail']['nome'])) ? $_SESSION['dadosEmail']['nome'] : "";
$remEmail = (isset($_SESSION['dadosEmail']['email'])) ? $_SESSION['dadosEmail']['email'] : "";

//Remetente diferente do login do servidor
$deNome = (isset($_SESSION['dadosEmail']['sessEnviarDeNome'])) ? $_SESSION['dadosEmail']['sessEnviarDeNome'] : $remNome;
$deEmail = (isset($_SESSION['dadosEmail']['sessEnviarDeEmail'])) ? $_SESSION['dadosEmail']['sessEnviarDeEmail'] : $remEmail;

?>
<h5>Autenticador</h5>
	<form action="app/CadSessUser.php" method="POST">
		<span>
			<input type="nome" placeholder="Nome" name="sessNome" value="<?=$remNome;?>">
		</span><br>

		<span>
			<input type="email" placeholder="E-mail" name="sessEmail" value="<?=$remEmail;?>">
		</span><br>

		<span>
			<input type="password" id='senhaOculta' placeholder="Senha" type="password" name="sessSenha" required>
			<label><input id='exibirsenha' type='checkbox' onclick='mostraSenha()'>Mostrar senha</label>
		</span><br>
		<hr>
		<label>
				<input id='outroRemetente' type='checkbox' onclick='mostrarDe()'>
				<strong id="labelOutroRemetente">Mostrar De:</strong>
				<span>(Opcional). Enviar de Outro E-mail<strong>*</strong>.<br>
					<sub>* Verifique esta opção no seu provedor de e-mail.</sub>
				</span>
		</label>
		<br><br>
		<style>
#formOutroRemetente {
    display: none;
}
</style>
		<div id="formOutroRemetente">

		<span>Caso os campos abaixo não forem preenchidos o remetente será o mesmo informado acima no autenticador</span><br>
		<span>
			<input type="text" placeholder="Nome do Remetente Diferente" name="sessEnviarDeNome">
		</span><br>
		<span>
			<input type="email" placeholder="E-mail" name="sessEnviarDeEmail">
		</span><br>

		<br>

		</div>
		<span>
			<input type="submit" value="Cadastrar">
		</span>
	</form>
	<script>
		function mostraSenha() {
	    // check if checkbox is checked
	    if (document.querySelector('#exibirsenha').checked) {
	      // if checked
	      document.querySelector('#senhaOculta').type = "text";
	    } else {
	      // if unchecked
	      document.querySelector('#senhaOculta').type = "password"
	    }
	  }
		function mostrarDe() {
	    // check if checkbox is checked
	    if (document.querySelector('#outroRemetente').checked) {
	      // if checked
	      document.getElementById("formOutroRemetente").style.display= 'unset';
	      document.querySelector('#labelOutroRemetente').textContent="Ocultar De:";
	    } else {
	      // if unchecked
	      document.getElementById("formOutroRemetente").style.display= 'none';
	      document.querySelector('#labelOutroRemetente').textContent="Mostrar De:";
	    }
	  }
	</script>
</body>
</html>