<?php
// require_once 'app/appConfig.php';
require_once 'bootstrap.php';
require_once 'menu.php';
ExigeRemetente();
if (isset($_POST['sessMensagem'])) {
	$_SESSION['dadosEmail']['mensagem'] = $_POST['sessMensagem'];
	FlashMessage('Mensagem atualizada!');
	GeraLog('Cadastrando Mensagem de sessão');
}
if (isset($_POST['limpaSessMensagem'])) {
	unset($_SESSION['dadosEmail']['mensagem']);
	FlashMessage('Mensagem Limpa!');
	GeraLog('Limpando Mensagem de sessão');
}
//Exige captcha
//chechaCaptcha();

echo "<h5>";
MostraFlashMessage();
echo "</h5>";
?>
<style>
.cinza {
    background: #8080804d;
    margin: 10px;
    line-height: 30px;
    padding: 3px;
}
.botaoVerde{
	background: green;
    color: white;
    font-size: 1.2em;
}
}
</style>

	<p>
		Esta mensagem será adicionada ao corpo do e-amil a ser enviado. Se ele estiver vazio não afetará o envio.
	</p>
	<p>
		Para criar uma mensagem personalizada, use as tags abaixo para compor sua mensagem:
		<ul>
			<li>
		%ClienteNome% = Usará os dados da 1ª coluna do arquivo CSV (obrigatório ser o Nome);
			</li>

			<li>
		%ClienteEmail% = Usará os dados da 2ª coluna do arquivo CSV (obrigatório ser o e-mail);
			</li>

			<li>
		%coluna4% = Usará os dados da 4ª coluna do arquivo CSV esta coluna poderá conter qualquer dados inclusive personalizado (se houver);
			</li>
		</ul>

		As tags podem ser usadas aqui ou diretamente no CSV, não importando se em maíuscula ou minúscula, o importante é ser uma das informadas aqui. <br>

		Ps:A mensagem (3ª coluna) do CSV tem PRIORIDADE sobre esta escrita aqui. Caso queira usar a mensagem da sessão (esta), deixe a 3ª coluna em branco como no exemplo 3:

			<!-- <pre> -->
				<ol>Exemplos:
				<li>1ªcoluna;2ªcoluna;3ªcoluna;4ªcoluna</li>
				<li>nome;email;mensagem;diversos</li>
				<li>nome;email;;diversos</li>
			</ol>
		<!-- </pre> -->

	</p>
	<form method="POST">
		<input name="limpaSessMensagem" type="submit" value="Limpar mensagem da sessão">
	</form>
	<form method="POST">
		Mensagem de sessão configurada:
		<?php
if (isset($_SESSION['dadosEmail']['mensagem'])) {
	echo "SIM, o e-mail é enviado com ESTA mensagem caso não conste uma no CSV.";
} else {
	echo "NÃO, o e-mail é enviado sem uma mensagem caso não conste no CSV.";

}
?>
		<textarea class="cinza" name="sessMensagem" cols="30" rows="10" style="width: 100%;height: 229px;"><?php
if (isset($_SESSION['dadosEmail']['mensagem'])) {
	echo $_SESSION['dadosEmail']['mensagem'];
} /**/else {
	echo "Exemplo:
Boa tarde sr(a) %ClienteNome%,
Seu e-mail é %ClienteEmail%.
%coluna4%";
} /**/
?></textarea>

		<span>
			<input class="botaoVerde" type="submit" value="Cadastrar/Atualizar">
		</span>
		<br>
	</form>
	<hr>

<?php
$paraNomeDemo = "Fulano de Tal";
$paraEmailDemo = "fulano@detal.com";
$coluna4Demo = "Atenciosamente, Tiago França";
$mensagem = (isset($_SESSION['dadosEmail']['mensagem'])) ? $_SESSION['dadosEmail']['mensagem'] : "Boa tarde sr(a) %ClienteNome%, seu e-mail é %ClienteEmail%. %coluna4%";
echo "
	<p> Dados fictícios:
		<ul>
			<li><span class='cinza'>%ClienteNome%</span> = Nome: <span class='cinza'>{$paraNomeDemo}</span></li>
			<li><span class='cinza'>%ClienteEmail%</span> = E-mail: <span class='cinza'>{$paraEmailDemo}</span></li>
			<li><span class='cinza'>%coluna4%</span> = 4ª Coluna Personalizada: <span class='cinza'>{$coluna4Demo}</span></li>
		</ul>
	Exemplo de como a mensagem atual ficará usando os dados acima:
	</p>";
$mensagem = str_ireplace("%ClienteNome%", $paraNomeDemo, "$mensagem");
$mensagem = str_ireplace("%ClienteEmail%", $paraEmailDemo, "$mensagem");
$mensagem = str_ireplace("%coluna4%", $coluna4Demo, "$mensagem");
$mensagem = nl2br($mensagem);
echo "<p class='cinza' style='margin:10px;'>

			{$mensagem}

	</p><br>";
?>
