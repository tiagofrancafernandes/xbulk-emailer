<?php
// require_once 'app/appConfig.php';
require_once 'bootstrap.php';
require_once 'menu.php';
ExigeRemetente();
if (isset($_POST['sessMensagem'])) {
	$_SESSION['dadosEmail']['multiItensSessao'] = $_POST['sessMensagem'];
	FlashMessage('Mensagem atualizada!');
	GeraLog('Cadastrando Mensagem de sessão');
}
if (isset($_POST['limpaMultiItensSessao'])) {
	unset($_SESSION['dadosEmail']['multiItensSessao']);
	FlashMessage('Mensagem Limpa!');
	GeraLog('Limpando Mensagem de sessão');
}
//Exige captcha
//chechaCaptcha();

echo "<h5>";
MostraFlashMessage();
echo "</h5>";
?>
<style>
.cinza {
    background: #8080804d;
    margin: 10px;
    line-height: 30px;
    padding: 3px;
}
.botaoVerde{
	background: green;
    color: white;
    font-size: 1.2em;
}
}
</style>

	<p>
		Se configurado, a mensagem Multi-ítens substituirá a do CSV;
	</p>
	<p>
		Vantagens de criar Multi-ítens na sessão:
		<ul>
			<li>
				Você economiza tempo na edição do CSV;
			</li>
			<li>
				O seu arquivo CSV fica menor e reutilizável no futuro;
			</li>
			<li>
				Uma lista de ítens padrão para todos, útil em uma promoção rápida onde pode-se colocar vários ítens um lado a lado com o outro;
			</li>

			<li>
				Você pode utilizar as variáveis semelhante à mensgaem de sessão tornando os e-mails mais personalizados;<br>
				Exemplo (Se o nome de um cliente for João e do outro Pedro):
				<pre>
					A mensagem <strong>"Obrigado %ClienteNome%!"</strong>
					Em um e-mail ficará: <strong>"Obrigado João"</strong>
					no outro ficará: <strong>"Obrigado Pedro"</strong>
				</pre>
			</li>

			<li>
				Pode colocar qualquer elemento HTML (tomando cuidado com as vírgulas, esta separam os elementos);
			</li>
		</ul>

	</p>
	<form method="POST">
		<input name="limpaMultiItensSessao" type="submit" value="Limpar mensagem da sessão">
	</form>
	<form method="POST">
		Mensagem de sessão configurada:
		<?php
if (isset($_SESSION['dadosEmail']['multiItensSessao'])) {
	echo "SIM, o e-mail é enviado com ESTA mensagem caso não conste uma no CSV.";
} else {
	echo "NÃO, o e-mail é enviado sem uma mensagem caso não conste no CSV.";

}
?>
		<textarea class="cinza" name="sessMensagem" cols="30" rows="10" style="width: 100%;height: 229px;"><?php
if (isset($_SESSION['dadosEmail']['multiItensSessao'])) {
	echo $_SESSION['dadosEmail']['multiItensSessao'];
} /**/else {
	echo "Exemplo:
Boa tarde sr(a) %ClienteNome%,
Seu e-mail é %ClienteEmail%.
%coluna4%";
} /**/
?></textarea>

		<span>
			<input class="botaoVerde" type="submit" value="Cadastrar/Atualizar">
		</span>
		<br>
	</form>
	<hr>

<?php
$paraNomeDemo = "Fulano de Tal";
$paraEmailDemo = "fulano@detal.com";
$varMultiItens = (isset($_SESSION['dadosEmail']['multiItensSessao'])) ? $_SESSION['dadosEmail']['multiItensSessao'] : "Boa tarde sr(a) %ClienteNome%, seu e-mail é %ClienteEmail%. %coluna4%";
echo "
	<p> Dados fictícios:
		<ul>
			<li><span class='cinza'>%ClienteNome%</span> = Nome: <span class='cinza'>{$paraNomeDemo}</span></li>
			<li><span class='cinza'>%ClienteEmail%</span> = E-mail: <span class='cinza'>{$paraEmailDemo}</span></li>
		</ul>
	Exemplo de como a mensagem atual ficará usando os dados acima:
	</p>";
$varMultiItens = str_ireplace("%ClienteNome%", $paraNomeDemo, "$varMultiItens");
$varMultiItens = str_ireplace("%ClienteEmail%", $paraEmailDemo, "$varMultiItens");
$varMultiItens = nl2br($varMultiItens);

//----------------------------------------------
$CSV2String = $varMultiItens;

// $dados = str_getcsv($CSV2String, ","); //analisa as linhas do arquivo
$quebraPadrao = 5;
$itensPorLinha = (int) $quebraPadrao;
$e = explode(",", $CSV2String);
$contaKey = count($e);
$c = 0;
$separaItens = "|";
$tmpar = " ";
foreach ($e as $chave => $valor) {
	$valor = str_replace("  ", "EspacO", "$valor"); //Elimina linhas vazias
	$valor = str_replace("EspacO", " ", "$valor"); //Elimina linhas vazias

	if ($valor == "" OR $valor == " ") {
		unset($valor);
		continue;
	}

	$chave2 = $chave + 1;
	$tmpar .= $valor;
	if ($c <= $contaKey AND $contaKey > 1) {
		$tmpar .= " " . $separaItens . " ";
	}
	$c++;
	if ($itensPorLinha != 0) {
		if ($chave2 == $itensPorLinha) {$tmpar .= "XXXX";}
		if ($chave2 % $itensPorLinha == 0) {
			$tmpar .= " " . $chave2 . " / " . $itensPorLinha . "<br>";
		}
	}
}
$varMultiItens = $tmpar;
var_dump($itensPorLinha);
echo "<hr>";
echo '$itensPorLinha: ' . $itensPorLinha;
//----------------------------------------------

echo "<hr>";
echo $varMultiItens;
?>
