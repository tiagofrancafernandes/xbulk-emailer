<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Listar E-mails</title>
</head>
<body>
<?php
// require_once 'app/appConfig.php';
require_once 'bootstrap.php';
require_once 'menu.php';

$caminhoArquivo = './pastacsv/novoListaDeEmails.csv';

if (isset($_GET['ExcluirLista']) AND $_GET['ExcluirLista'] == 'sim') {
	// unlink('./pastacsv/novoListaDeEmails.csv');
	if (is_writable($caminhoArquivo)) {
		file_put_contents($caminhoArquivo, "");
		chmod($caminhoArquivo, 0777);
		irPara('Listar_Emails_Cadastrados.php');
	} else {
		FlashMessage('Sem permissão para excluir a lista de e-mails.<br> Verifique as permissões no arquivo.');
		irPara('Listar_Emails_Cadastrados.php');
	}
}

MostraFlashMessage();

if (file_exists($caminhoArquivo)) {

	$CSV2String = file_get_contents($caminhoArquivo);
	$CSV2String = str_ireplace("\n\n", "", "$CSV2String"); //Elimina linhas vazias
	$dados = str_getcsv($CSV2String, "\n"); //analisa as linhas do arquivo
	if ($CSV2String != "") {
		echo "<a href='Listar_Emails_Cadastrados.php?ExcluirLista=sim' onclick=\"if (! confirm('Deseja mesmo EXCLUIR a lista de e-mails?')) { return false; }\" style='color:red;'> Excluir  a lista de e-mails</a> || <a href='./EnviaCSV_Emails.php'>Atualizar Lista de Emails</a><hr>";
		echo "<h5>Lista de E-mails Cadastrados | <a href='{$caminhoArquivo}' target='_blank'>Baixar lista atual<a> | Total de destinatários: " . count($dados) . "</h5>";} else {
		echo "Não há lista de E-mails <br>";
		echo "<a href='./EnviaCSV_Emails.php'>Cadastrar Lista de Emails</a>";
	}
	if (strpos($CSV2String, "\n\n") !== false) {
		echo "Há linhas vazias na sua lista de e-mail!";
	} else {
		foreach ($dados as &$linha) {
			$linha = str_getcsv($linha, ";");
			if (isset($linha[0]) AND isset($linha[1])) {

				$paraNome = $linha[0];
				$paraEmail = $linha[1];
				if (isset($linha[2]) AND $linha[2] != "") {
					//Checa se há uma mensagem no arquivo CSV, se não, usa a mensagem configurada na sessão;
					$mensagem = $linha[2];
				} elseif (isset($_SESSION['dadosEmail']['mensagem'])) {
					$mensagem = $_SESSION['dadosEmail']['mensagem'];
				} else {
					$mensagem = "";
				}
				if (isset($linha[3])) {
					//Checa se há uma quarta coluna no CSV, ela será adicionada em qualquer parte da mensagem e seu conteúdo pode variar e pode ser diferente para cada destinatário;
					$coluna4 = $linha[3];
				} else {
					$coluna4 = "";
				}

				/*
						$mensagem = nl2br($mensagem);
						//str_ireplace Ignora letras maiúsculas de minúsculas
						$mensagem = str_ireplace("%ClienteNome%", $paraNome, "$mensagem");
						$mensagem = str_ireplace("%ClienteEmail%", $paraEmail, "$mensagem");
						$mensagem = str_ireplace("%coluna4%", $coluna4, "$mensagem");
					*/
				$mensagem = (isset($linha[2])) ? "CSV" : "SESSÃO";

				echo "<hr> Nome: " . $paraNome . " | E-mail: " . $paraEmail . "<hr>";
			}
		}
	}
} else {
	echo "Não há lista de E-mails <br>";
	echo "<a href='./EnviaCSV_Emails.php'>Cadastrar Lista de Emails</a>";
}

?>
</body>
</html>