<?php
require_once 'app/appConfig.php';
require_once 'menu.php';

if (isset($_GET['sair'])) {
	if (isset($_SESSION['dadosEmail'])) {
		unset($_SESSION['dadosEmail']);
		// unset($_SESSION['dadosEmail']['email']);
		// unset($_SESSION['dadosEmail']['senha']);
		// unset($_SESSION['dadosEmail']['estado']);
		FlashMessage('Dados do remetente, porém a lista de e-mails continua salva.');
		header("location:" . ROOTAPP . "Cadastrar_Remetente.php");
	} else {
		ExigeRemetente();
	}
}

if (isset($_GET['sairEExcluir'])) {
	if (isset($_SESSION['dadosEmail'])) {
		unset($_SESSION['dadosEmail']);
		// unset($_SESSION['dadosEmail']['email']);
		// unset($_SESSION['dadosEmail']['senha']);
		// unset($_SESSION['dadosEmail']['estado']);
		$caminhoArquivo = './pastacsv/novoListaDeEmails.csv';
		// unlink($caminhoArquivo);
		file_put_contents($caminhoArquivo, "");
		chmod($caminhoArquivo, 0777);
		FlashMessage('Dados do remetente e lista de e-mails limpos.');
		header("location:" . ROOTAPP . "Cadastrar_Remetente.php");
	} else {
		ExigeRemetente();
	}
}

echo "Deseja apenas <span style='color:blue;'>sair</span> ou <span style='color:red;'>sair e excluir</span> a lista de e-mails ?";
echo "<br>";
echo "<a href='Sair.php?sair'> Sair apenas</a> || ";
echo "<a href='Sair.php?sairEExcluir' onclick=\"if (! confirm('Deseja mesmo EXCLUIR a lista de e-mails?')) { return false; }\" style='color:red;'> Sair e excluir  a lista de e-mails</a> <br>";
?>