<?php
/*
Tiago França | contato@tiagofranca.com
 */

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

require_once 'vendor/autoload.php';
// require_once 'app/appConfig.php'; //Migrar para o composer?
require_once '../bootstrap.php';
require_once '../menu.php'; //Migrar para o composer?
require_once 'dadosConfig.php';

$horaInicioEnvio = date('d/M/Y H:i:s');
$statusEnvia = "Iniciando";
if (isset($_SESSION['dadosEmail']) AND isset($_SESSION['dadosEmail']['senha']) AND isset($_SESSION['dadosEmail']['nome'])) {

	if (isset($_SESSION['statusCaptcha']) AND $_SESSION['statusCaptcha'] === "CapAprovado") {

		$mail = new PHPMailer(true);

		$arquivo = '../pastacsv/novoListaDeEmails.csv';
//Pega os dados do arquivo csv
		$CSV2String = file_get_contents($arquivo);
		$CSV2String = str_ireplace("\n\n", "", "$CSV2String"); //Elimina linhas vazias

		$dados = str_getcsv($CSV2String, "\n"); //analisa as linhas do arquivo
		$dados2 = $dados;

		$totalDestinatarios = count($dados);

		if ($CSV2String != "") {
			echo "<a href='Listar_Emails_Cadastrados.php?ExcluirLista=sim' onclick=\"if (! confirm('Deseja mesmo EXCLUIR a lista de e-mails?')) { return false; }\" style='color:red;'> Excluir  a lista de e-mails</a> || <a href='./EnviaCSV_Emails.php'>Atualizar Lista de Emails</a><hr>";
		} else {
			echo "Não há lista de E-mails <br>";
			echo "<a href='./EnviaCSV_Emails.php'>Cadastrar Lista de Emails</a>";
		}
		if (strpos($CSV2String, "\n\n") !== false) {
			$msg = "Há linhas vazias na sua lista de e-mail!";
			echo $msg;
			FlashMessage($msg . "\n");
		} else {
			$contaLinha = 0;
			foreach ($dados as &$linha) {
				$contaLinhaARR = $contaLinha;
				$linha = str_getcsv($linha, ";");
				//----------INICIO DA VALIDAÇÃO DE NOME E E-MAIL DE DESTINO ---------------------

				if ($linha[0] == " " OR $linha[0] == "") {
					FlashMessage("Há dados incorreto no CSV, Estes não foram enviados. Favor verificar (NOME OU E-MAIL INCORRETOS OU AUSENTES)\n");
					unset($linha[0]);
					unset($dados[$contaLinha]);
					/*continue;*/}
				// if (strpos($linha[0], ' ') !== false) {unset($linha[0]);} //Verifica se o primeiro campo tem alguma informação
				if (checaSeEmail($linha[1]) == false) {
					FlashMessage("Há dados incorreto no CSV, Estes não foram enviados. Favor verificar (NOME OU E-MAIL INCORRETOS OU AUSENTES)\n");
					unset($linha[1]);
					unset($dados[$contaLinha]);
					/*continue;*/} //Verifica se o primeiro campo é um e-mail, se for a chave é anulada

				echo ($contaLinhaARR . $linha[0]);
				echo (" " . $linha[1]);
				$contaLinha++;
				//----------FIM DA VALIDAÇÃO DE NOME E E-MAIL DE DESTINO ---------------------

				if (isset($linha[0]) AND isset($linha[1])) {
					$paraNome = $linha[0];
					$paraEmail = $linha[1];
					if (isset($linha[2]) AND $linha[2] != "") {
						//Checa se há uma mensagem no arquivo CSV, se não, usa a mensagem configurada na sessão;
						$mensagem = $linha[2];
					} elseif (isset($_SESSION['dadosEmail']['mensagem'])) {
						$mensagem = $_SESSION['dadosEmail']['mensagem'];
					} else {
						$mensagem = "";
					}
					// var_dump($linha);
					//-----------------------------------------
					if (isset($linha[3])) {
						//Checa se há uma quarta coluna no CSV, que informa a quebra por linha
						$quebraPadrao = (int) 3; //Valor padrão da quebra

						if (strpos($linha[3], ',') !== false) {
//Verifica se há vírgula onde deveria estar a quebra, caso tenha uma vírgula, o sistema entende que esta coluna foi esquecida e atribui uma quebra e muda a posição do coluna de dados no CSV
							// $linha[4] = $linha[3];
							$linha[4] = $linha[3];
							$linha[3] = $quebraPadrao; //Atribui o valor padrão
						}
						$itensPorLinha = $linha[3];
						$itensPorLinha = ($itensPorLinha == "" OR $itensPorLinha == " ") ? (int) $quebraPadrao : (int) $itensPorLinha;

						$itensPorLinha = (is_int($itensPorLinha)) ? $itensPorLinha : $quebraPadrao;
					} else {
						$itensPorLinha = (int) 0;
					}

					$multiItens = (isset($linha[4])) ? $linha[4] : "";
					$separaItens = (isset($linha[5])) ? (string) $linha[5] : "";

					$e = explode(",", $multiItens);
					$contaKey = count($e);
					$c = 0;
					$tmpar = "";
					foreach ($e as $chave => $valor) {
						$chave2 = $chave + 1;
						$tmpar .= $valor;
						if ($c <= $contaKey AND $contaKey > 1) {
							$tmpar .= " " . $separaItens . " ";
						}
						$c++;
						if ($itensPorLinha != 0) {
							if ($chave2 % $itensPorLinha == 0) {
								$tmpar .= "<br>";
							}
						}
					}
					//-----------------------------------------
					$parteFinal = "<br>\n<hr>\n aqui vai o final do e-mail;";
					$mensagem = $mensagem;
					$mensagem = nl2br($mensagem);
					//str_ireplace Ignora letras maiúsculas de minúsculas
					// $mensagem = str_ireplace("%multiItens%", $multiItens, "$mensagem");
					$mensagem = str_ireplace("%ClienteNome%", $paraNome, "$mensagem");
					$mensagem = str_ireplace("%ClienteEmail%", $paraEmail, "$mensagem");
					$tmpar = ($tmpar != "") ? $tmpar . "<hr>" : $tmpar;
					$mensagem = $mensagem . "<br>" . $tmpar . $parteFinal;

					try {

						//Server settings
						$mail->SMTPDebug = 2; // Enable verbose debug output
						$mail->isSMTP(); // Set mailer to use SMTP
						$mail->Host = 'smtp.office365.com'; // Specify main and backup SMTP servers
						$mail->SMTPAuth = true; // Enable SMTP authentication
						$mail->Username = $usuEmail; // SMTP username
						$mail->Password = $usuSenha; // SMTP password
						$mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
						$mail->Port = 587; // TCP port to connect to
						$mail->CharSet = "UTF-8";

						//Remetente diferente do login do servidor
						$deNome = (isset($_SESSION['dadosEmail']['sessEnviarDeNome'])) ? $_SESSION['dadosEmail']['sessEnviarDeNome'] : $remNome;
						$deEmail = (isset($_SESSION['dadosEmail']['sessEnviarDeEmail'])) ? $_SESSION['dadosEmail']['sessEnviarDeEmail'] : $remEmail;
						//Recipients
						$mail->setFrom($deEmail, $deNome);
						// $mail->addAddress('tiago.franca.ext@sascar.com.br', 'Tiago T'); // Add a recipient
						// $mail->addAddress('tiago.franca.ext@sascar.com.br'); // Name is optional
						$mail->addReplyTo($deEmail, $deNome);
						// $mail->addCC('devtiagofranca@gmail.com');

						$mail->addBCC($paraEmail, $paraNome);
						//Attachments
						// $mail->addAttachment('/var/tmp/file.tar.gz'); // Add attachments
						// $mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name

						//Content
						$mail->isHTML(true); // Set email format to HTML
						$mail->Subject = "Assunto teste {$paraNome}";
						$emailMensagem = "Para: <b>{$paraNome}</b> - E-mail: {$paraEmail} \n<hr> {$mensagem}\n";
						$mail->Body = $emailMensagem;
						$mail->AltBody = $emailMensagem;

						// $mail->send();
						// echo 'Assunto: ' . $mail->Subject . ' <br>';
						// echo 'Corpo: ' . $mail->Body . ' <hr><br>';
						// // echo 'Mensagem enviada! <br>';

						//Finalmente envia!
						$mail->Send();
						$statusEnvia = "DEV";
						$statusEnvia = "ok"; /**
						echo "<br>" . $mensagem; /**/
					} catch (Exception $e) {
						echo 'A Mensagem não pode ser enviada. Verifique os dados informados tanto  do remetente quanto do servidor de envio.<br>';
						// echo 'Erro: ' . $mail->ErrorInfo;
						GeraLog('Erro no envio');
						// GeraLog("A Mensagem não pode ser enviada \nErro: " . $mail->ErrorInfo);
						$statusEnvia = "falha";
					}

					//Limpa destinatarios, mensagens e anexos;
					$mail->ClearAllRecipients();
					$mail->ClearAttachments();
				}
				//Limpa: $_SESSION['statusCaptcha']
				limpaStatusCaptcha();

				if ($statusEnvia == "ok") {
					$horaFimEnvio = date('d/M/Y H:i:s');
					GeraLog("Enviando E-mails.\n Total de destinatarios:" . count($dados));
					$msgSucessoFim = "Emails enviados!\nInicio do envio: {$horaInicioEnvio}\n Fim do envio: {$horaFimEnvio} \nTotal de destinatarios: " . $totalDestinatarios;
					FlashMessage($msgSucessoFim);
					GeraLog($msgSucessoFim);
					$_SESSION['dadosEmail']['emailsEnviados'] = $msgSucessoFim;
					irParaHTML(ROOTAPP . 'MensagensEnviadas.php');
				}
			}

			$dadosTo = count($dados) - 1;
			echo "<h5>Lista de E-mails Cadastrados | <a href='{$arquivo}' target='_blank'>Baixar lista atual<a> | Total de destinatários: " . $dadosTo . "</h5>";
		}

	} else {
		//Manda para o captcha se não suprir: $_SESSION['statusCaptcha'] === "CapAprovado"
		$msg = "Falha no capthca, Informe um captcha válido!";
		GeraLog($msg);
		FlashMessage("<span style='color:red;'>" . $msg . "</span>");
		formUrlCaptcha();
	}
} else {
	//Manda para o cadastro de remetente se este não tiver sido devidamente informado
	$msg = "Sem remetente cadastrado ou com dados incorretos";
	GeraLog($msg);
	FlashMessage("<span style='color:red;'>" . $msg . "</span>");
	header("location:" . CADFROM);
}

?>
