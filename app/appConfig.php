<?php
/*
Criado em: 05/12/2018
Por: Tiago França Fernandes
Atualizado em:
 */

//Testa sessão, se não, inicia
if (!isset($_SESSION)) {
	session_start();
}

ini_set('max_execution_time', 120);
set_time_limit(120);

define('ROOTAPP', "http://" . $_SERVER['SERVER_NAME'] . "/git_projetos/exchangemail_rafael/");

define('CADFROM', ROOTAPP . "Cadastrar_Remetente.php");

$ESTA_URL = "http://" . $_SERVER['PHP_SELF'];

$HORA = date('d/M/Y H:i:s');

/*
INÍCIO DAS FUNÇÕES
 */

//Cria uma mensagem na sessão
function FlashMessage($msg) {
	if (!isset($_SESSION)) {
		session_start();
	}
	$_SESSION['flashMSG'] = (isset($_SESSION['flashMSG'])) ? $_SESSION['flashMSG'] = $_SESSION['flashMSG'] : $msg;
	return $_SESSION['flashMSG'] = $_SESSION['flashMSG'];
}

//Mostra a mensagem se existir, em seguida a limpa da sessão,se atualizar a página ela some
function MostraFlashMessage() {
	if (!isset($_SESSION)) {
		session_start();
	}
	if (isset($_SESSION['flashMSG'])) {
		$_SESSION['flashMSG'] = (string) $_SESSION['flashMSG'];
		echo "<strong>";
		echo nl2br($_SESSION['flashMSG']);
		echo "</strong><br><br>";
		unset($_SESSION['flashMSG']);
	}
}

//Exige um remetente na página onde for chamada
function ExigeRemetente() {
	if (!isset($_SESSION['dadosEmail'])) {
		FlashMessage('Não há remetente cadastrado');
		header("location:" . ROOTAPP . "Cadastrar_Remetente.php");
	}
}

function ocultaLetras($texto) {
	$texto = str_ireplace('a', '*', $texto);
	$texto = str_ireplace('e', '*', $texto);
	$texto = str_ireplace('i', '*', $texto);
	$texto = str_ireplace('u', '*', $texto);
	$texto = str_ireplace('n', '*', $texto);
	$texto = str_ireplace('b', '*', $texto);
	$texto = str_ireplace('r', '*', $texto);
	$texto = str_ireplace('s', '*', $texto);
	return $texto;

}

//Gera arquivo de Log
// $acaoLog Informa a ação executada para colocar no Log
function GeraLog($acaoLog = "Log simples", $nomeArquivoLog = "logs") {
	$pastaLogs = ROOTSITE . "logs/";
	$arquivoLog = $pastaLogs . $nomeArquivoLog . ".txt";
// $acaoLog Informa a ação executada para colocar no Log
	global $HORA;
	$acaoLog = $acaoLog;
	$nomeLog = $_SESSION['dadosEmail']['nome'];
	$emailLog = $_SESSION['dadosEmail']['email'];
	// $emailLog = $emailLog;
	$emailLog = ocultaLetras($emailLog);
	$horaLog = $HORA;

	//Libera a variável
	if (isset($_SESSION['AcaoLog'])) {
		unset($_SESSION['AcaoLog']);
	}

	$conteudoLog = "----------------------\n$horaLog | Remetente = $nomeLog, $emailLog,\n	Origem do log: http:/" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "\n	Ação: $acaoLog\n----------------------";

//Em MegaBytes
	$tamanhoMaximoMB = 3;
	$tamanhoMaximo = $tamanhoMaximoMB * 1000000;
	$horaLog = date('Ymd_H_i_s');
	if (file_exists($arquivoLog)) {
		$tamanho = filesize($arquivoLog);
		$novoNome = $arquivoLog . "_bkp_" . $horaLog;
		if ($tamanho >= $tamanhoMaximo) {
			echo "Maior que " . $tamanhoMaximo;
			echo "<br>";
			rename($arquivoLog, $novoNome); //Renomeia o arquivo quando ele excede o limite $tamanhoMaximoMB
		}
	} else {
		$conteudoLogInicial = "----------------------------------\nIniciando o arquivo de LOG\n----------------------------------\n";
		file_put_contents($arquivoLog, $conteudoLogInicial, FILE_APPEND);
		chmod($arquivoLog, 0777);
	}

	if (!file_exists($arquivoLog)) {
		file_put_contents($arquivoLog, $conteudoLog);
		chmod($arquivoLog, 0777);
	}
	if (is_writable($arquivoLog)) {
		file_put_contents($arquivoLog, $conteudoLog, FILE_APPEND);
	} else {
		echo "Falha na criação do arquivo de Log: SEM PERMISSÃO.";
	}
}

//Exige captcha
// formUrlCaptcha();
// Envia o usuário para o formulário do captcha formUrlCaptcha();
function formUrlCaptcha($MSG_CAPTCHA = "Valide o capthca!", $formCaptcha = ROOTAPP . "app/formCaptcha.php") {
	unset($_SESSION['statusCaptcha']);
	$_SESSION['erroCapcha'] = $MSG_CAPTCHA;
	header("location:" . $formCaptcha . "?goto=" . $_SERVER['PHP_SELF']);
}
function limpaStatusCaptcha() {
	if (isset($_SESSION['statusCaptcha'])) {
		unset($_SESSION['statusCaptcha']);
	}
}

function irPara($para, $de = "") {
	if (isset($de) AND $de != "") {
		header("location:" . $para . "?de=" . $de);
	} else {
		header("location:" . $para);
	}
}

function irParaHTML($para, $de = "") {
	if (isset($de) AND $de != "") {
		echo "<meta http-equiv='refresh' content='0; url=" . $para . "?de=" . $de . "'>";
	} else {
		echo "<meta http-equiv='refresh'content='0; url=" . $para . "'>";
	}
}
function checaSeEmail($email) {
	$conta = "/^[a-zA-Z0-9\._-]+@";
	$domino = "[a-zA-Z0-9\._-]+.[.]";
	$extensao = "([a-zA-Z]{2,4})$/";
	$pattern = $conta . $domino . $extensao;
	if (preg_match($pattern, $email)) {
		return true;
	} else {
		return false;
	}
	return $email;
}
/*
FIM DAS FUNÇÕES
 */

?>