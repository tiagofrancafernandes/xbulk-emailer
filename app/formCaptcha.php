<?php
//Remover depois com a importação do appConfig.php
if (!isset($_SESSION)) {
	session_start();
}
require 'vendor/autoload.php';
require_once 'appConfig.php'; //Migrar para o composer?
require_once '../menu.php'; //Migrar para o composer?
require 'dadosConfig.php';

MostraFlashMessage();

$destino = (isset($_GET['goto'])) ? $_GET['goto'] : "home.php";
if (isset($_POST['valorCapcha'])) {

	if (isset($_SESSION['somaCapcha'])) {
		if ($_SESSION['somaCapcha'] == $_POST['valorCapcha']) {
			$_SESSION['statusCaptcha'] = "CapAprovado";
			$_SESSION['erroCapcha'] = "Captcha INCORRETO!";
			if (isset($destino)) {
				GeraLog('Captcha com sucesso. Destino:' . $destino);
				header("location:" . $destino);
			}

		} else {
			$_SESSION['erroCapcha'] = "Captcha INCORRETO!";
		}
	} else {
		echo "Favor informar um captcha!";
	}
}

if (isset($_SESSION['somaCapcha'])) {
	unset($_SESSION['somaCapcha']);
}

$num1 = rand(1, 9);
$num2 = rand(1, 9);
$soma = $num1 + $num2;
echo $num1 . " + " . $num2 . "<br>";
$_SESSION['somaCapcha'] = $soma;
echo "Insira o resultado da soma acima:";
?>
<form method="POST">
<?php
if (isset($_SESSION['erroCapcha'])) {
	echo "<span style='color:red;'>" . $_SESSION['erroCapcha'] . "</span><br>";
	unset($_SESSION['erroCapcha']);
}
?>
	<input name="valorCapcha" type="text">
	<button type="submit">Verificar</button>

</form>