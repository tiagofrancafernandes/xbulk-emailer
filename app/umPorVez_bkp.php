<?php
/*
Tiago França | contato@tiagofranca.com
 */

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

require_once 'vendor/autoload.php';
// require_once 'app/appConfig.php'; //Migrar para o composer?
require_once '../bootstrap.php';
require_once '../menu.php'; //Migrar para o composer?
require_once 'dadosConfig.php';

//Exige captcha
// chechaCaptcha();
if (isset($_SESSION['dadosEmail']) AND isset($_SESSION['dadosEmail']['senha']) AND isset($_SESSION['dadosEmail']['nome'])) {

	if (isset($_SESSION['statusCaptcha']) AND $_SESSION['statusCaptcha'] === "CapAprovado") {

		$mail = new PHPMailer(true);

		$arquivo = '../pastacsv/novoListaDeEmails.csv';
//Pega os dados do arquivo csv
		$CSV2String = file_get_contents($arquivo);

		$dados = str_getcsv($CSV2String, "\n"); //analisa as linhas do arquivo

		$totalDestinatarios = count($dados);

		GeraLog("Enviando E-mails.\n Total de destinatarios:" . count($dados));
		foreach ($dados as &$linha) {
			$linha = str_getcsv($linha, ";");
			//echo $linha[2] . '<br>';
			$paraNome = $linha[0];
			$paraEmail = $linha[1];
			$mensagem = $linha[2];

			try {

				//Server settings
				$mail->SMTPDebug = 2; // Enable verbose debug output
				$mail->isSMTP(); // Set mailer to use SMTP
				$mail->Host = 'smtp.office365.com'; // Specify main and backup SMTP servers
				$mail->SMTPAuth = true; // Enable SMTP authentication
				$mail->Username = $usuEmail; // SMTP username
				$mail->Password = $usuSenha; // SMTP password
				$mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 587; // TCP port to connect to
				$mail->CharSet = "UTF-8";

				//Recipients
				$mail->setFrom('tiago.franca.ext@sascar.com.br', 'Sascar E-mail em massa');
				// $mail->addAddress('tiago.franca.ext@sascar.com.br', 'Tiago T'); // Add a recipient
				// $mail->addAddress('tiago.franca.ext@sascar.com.br'); // Name is optional
				$mail->addReplyTo('tiago.franca.ext@sascar.com.br', 'Tiago França Sender');
				// $mail->addCC('devtiagofranca@gmail.com');

				$mail->addBCC($paraEmail, $paraNome);
				//Attachments
				// $mail->addAttachment('/var/tmp/file.tar.gz'); // Add attachments
				// $mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name

				//Content
				$mail->isHTML(true); // Set email format to HTML
				$mail->Subject = "Assunto teste {$paraNome}";
				$emailMensagem = "Para: <b>{$paraNome}</b> - E-mail: {$paraEmail} <br> {$mensagem}";
				$mail->Body = $emailMensagem;
				$mail->AltBody = $emailMensagem;

				// $mail->send();
				// echo 'Assunto: ' . $mail->Subject . ' <br>';
				// echo 'Corpo: ' . $mail->Body . ' <hr><br>';
				// // echo 'Mensagem enviada! <br>';
				//Finalmente envia!
				$mail->Send();
				$statusEnvia = "ok";
			} catch (Exception $e) {
				echo 'A Mensagem não pode ser enviada. Verifique os dados informados tanto  do remetente quanto do servidor de envio.<br>';
				// echo 'Erro: ' . $mail->ErrorInfo;
				GeraLog('Erro no envio');
				// GeraLog("A Mensagem não pode ser enviada \nErro: " . $mail->ErrorInfo);
				$statusEnvia = "falha";
			}

			//Limpa destinatarios, mensagens e anexos;
			$mail->ClearAllRecipients();
			$mail->ClearAttachments();
		}
		//Limpa: $_SESSION['statusCaptcha']
		limpaStatusCaptcha();

		if ($statusEnvia === "ok") {
			$msgSucessoFim = "Emails enviados!\n Total de destinatarios: " . $totalDestinatarios;
			FlashMessage($msgSucessoFim);
			GeraLog($msgSucessoFim);
			// irPara('MensagensEnviadas.php', $ESTA_URL);
		}

	} else {
		//Manda para o captcha se não suprir: $_SESSION['statusCaptcha'] === "CapAprovado"
		$msg = "Falha no capthca, Informe um capthca válido!";
		GeraLog($msg);
		FlashMessage("<span style='color:red;'>" . $msg . "</span>");
		formUrlCaptcha();
	}
} else {
	//Manda para o cadastro de remetente se este não tiver sido devidamente informado
	$msg = "Sem remetente cadastrado ou com dados incorretos";
	GeraLog($msg);
	FlashMessage("<span style='color:red;'>" . $msg . "</span>");
	header("location:" . CADFROM);
}

?>