<?php

// require_once 'app/appConfig.php';
require_once 'bootstrap.php';
require_once 'menu.php';

$destino = "./pastacsv/";

ExigeRemetente();
if (isset($_FILES['arquivo']) AND ($_FILES['arquivo']['size'] !== 0) AND ($_FILES['arquivo']['type'] == "text/csv")) {
	//var_dump($_FILES);
	global $destino;

	$nomeArquivo = isset($_FILES['arquivo']) ? 'novoListaDeEmails.csv' : 'novoListaDeEmails.csv';
	$nomeFinal = $nomeArquivo;
	$tam = isset($_FILES['arquivo']) ? $_FILES['arquivo']['size'] : 0;
	$tipo = isset($_FILES['arquivo']) ? $_FILES['arquivo']['type'] : '';
	$tmp = isset($_FILES['arquivo']) ? $_FILES['arquivo']['tmp_name'] : '';
	$caminhoArquivo = $destino . '/' . $nomeFinal;

	echo "<h5>";
	MostraFlashMessage();
	echo "</h5>";
	if (!file_exists($caminhoArquivo)) {
		GeraLog('naoexiste');
		echo "<p>Não existe o arquivo, vou tentar criá-lo. Caso não apareça abaixo, verifique as permissões e tente novamente.<p/>";
		$conteudoTemporario = file_get_contents($tmp);
		file_put_contents($caminhoArquivo, $conteudoTemporario);
		chmod($caminhoArquivo, 0777);
		if ($tam > 0) {
			move_uploaded_file($tmp, $caminhoArquivo);
			chmod($caminhoArquivo, 0777);
		}
	}

	if (is_writable($caminhoArquivo)) {
		if ($tam > 0) {
			move_uploaded_file($tmp, $caminhoArquivo);
			chmod($caminhoArquivo, 0777);
			$msg = "Lista de e-mails atualizada!";
			FlashMessage($msg);
			unset($msg);
		}
	} else {
		$msg = "<p>Não é possível escrever no arquivo.<br>Verifique as permissões da pasta.</p>";
		echo $msg;
		GeraLog($msg);
	}

	if (is_readable($caminhoArquivo)) {
		irPara("Listar_Emails_Cadastrados.php");
		echo "<h5>Prévia dos e-mails:</h5>";

		$CSV2String = file_get_contents($caminhoArquivo);

		$CSV2String = str_ireplace("\n\n", "", "$CSV2String");
		if (strpos($CSV2String, "\n\n") !== false) {echo "Há linhas vazias na sua lista de e-mail!";} else {
			$dados = str_getcsv($CSV2String, "\n"); //analisa as linhas do arquivo
			echo "Total de destinatários: " . count($dados) . "<hr>";
			foreach ($dados as &$linha) {
				$linha = str_getcsv($linha, ";");
				//echo $linha[2] . '<br>';
				$paraNome = $linha[0];
				$paraEmail = $linha[1];
				$mensagem = (isset($linha[2])) ? "CSV" : "SESSÃO";
				echo "<hr> Nome: " . $paraNome . " | E-mail: " . $paraEmail . " <br>Mensagem: " . $mensagem . "<br>";
			}
		}

	} else {
		$msg = "Sem permissões de LEITURA no arquivo e/ou na pasta destino";
		FlashMessage($msg);
		unset($msg);
	}

} else {
	echo "<hr>";
	echo "Dados incorretos ou não enviados<br>";
}

echo "<hr>";
MostraFlashMessage();
echo "<a href='./EnviaCSV_Emails.php'>Atualizar Lista de Emails</a><br>";
?>