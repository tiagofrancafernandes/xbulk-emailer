<?php
require_once 'app/appConfig.php';

$caminhoArquivo = './pastacsv/novoListaDeEmails.csv';

if (file_exists($caminhoArquivo)) {
	$CSV2String = file_get_contents($caminhoArquivo);

	$dados = str_getcsv($CSV2String, "\n"); //analisa as linhas do arquivo
	foreach ($dados as &$linha) {
		$linha = str_getcsv($linha, ";");
		//echo $linha[2] . '<br>';
		$paraNome = $linha[0];
		$paraEmail = $linha[1];
		if (isset($linha[2]) AND $linha[2] != "") {
			//Checa se há uma mensagem no arquivo CSV, se não, usa a mensagem configurada na sessão;
			$mensagem = $linha[2];
		} else {
			$mensagem = $_SESSION['dadosEmail']['mensagem'];
		}
		if (isset($linha[3])) {
			//Checa se há uma quarta coluna no CSV, ela será adicionada em qualquer parte da mensagem e seu conteúdo pode variar e pode ser diferente para cada destinatário;
			$coluna4 = $linha[3];
		} else {
			$coluna4 = "";
		}

		//str_ireplace Ignora letras maiúsculas de minúsculas
		$mensagem = str_ireplace("%ClienteNome%", $paraNome, "$mensagem");
		$mensagem = str_ireplace("%ClienteEmail%", $paraEmail, "$mensagem");
		$mensagem = str_ireplace("%coluna4%", $coluna4, "$mensagem");

		var_dump($mensagem);
		//$mensagem = $_SESSION['dadosEmail']['mensagem'];
		echo "<br> Nome: " . $paraNome . " | E-mail: " . $paraEmail . " <br>Mensagem: " . $mensagem . "<br><hr>";
	}
} else {
	echo "Não há lista de E-mails <br>";
	echo "<a href='./EnviaCSV_Emails.php'>Cadastrar Lista de Emails</a>";
}

/*///REPLACE TEST*/

// $paginas = array(
// 	'Um' => ' %num% = 1',
// 	'Dois' => ' %num% = 2',
// 	'Tres' => ' %num% = 3',
// );

// //var_dump($paginas);
// $atual = 0;
// foreach ($paginas as $pagina => $codigo) {

// 	$codigo = str_ireplace("%num%", "$pagina", "$codigo");
// 	$bodytag = str_ireplace("%cor%", "red", "<p style='color:%COR%;'>Paragrafo! </p>");
// 	echo $bodytag . "<br>";
// 	echo $codigo . "<br>";
// 	$atual++;

// }

?>