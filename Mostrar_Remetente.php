<?php

require_once 'app/appConfig.php';
require_once 'menu.php';

if (!isset($_SESSION)) {
	session_start();
}
if (isset($_SESSION['dadosEmail']) AND isset($_SESSION['dadosEmail']['senha']) AND isset($_SESSION['dadosEmail']['nome'])) {

	echo "Nome: ";
	echo $_SESSION['dadosEmail']['nome'];
	echo "<br>";
	echo "E-mail: ";
	echo $_SESSION['dadosEmail']['email'];
	echo "<br>";
	echo "<input type='password' id='senhaOculta' value='" . $_SESSION['dadosEmail']['senha'] . "'disabled>";
	echo "<label><input id='exibirsenha' type='checkbox' onclick='mostraSenha()'>Mostrar senha</label>";
	echo " <a href='" . ROOTAPP . "Cadastrar_Remetente.php'>Atualizar dados do Remetente</a>";
	echo "<br>";
	echo "Estado: ";
	echo $_SESSION['dadosEmail']['estado'];

} else {
	ExigeRemetente();
	// FlashMessage('Não há remetentes cadastrados');
	//header("location:" . ROOTAPP . "Cadastrar_Remetente.php");
}
?>
<script>
	function mostraSenha() {
    // check if checkbox is checked
    if (document.querySelector('#exibirsenha').checked) {
      // if checked
      document.querySelector('#senhaOculta').type = "text";
    } else {
      // if unchecked
      document.querySelector('#senhaOculta').type = "password"
    }
  }
</script>